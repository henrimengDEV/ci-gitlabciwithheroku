class Test {

    constructor(message) {
        this.message = message
    }

    display() {
        return this.message
    }

    static add(a, b) {
        return a + b;
    }

    static multiply(a, b) {
        return a * b;
    }
}

module.exports = Test